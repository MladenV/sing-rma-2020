package com.example.v3p1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Kontakt> kontakti;
    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = findViewById(R.id.mainLayout);

        setupData();
        generateData();
    }

    private void setupData(){
        kontakti = new ArrayList<Kontakt>();
        for(int i = 0; i<50; i++){
            kontakti.add(new Kontakt("Ime" + i, "Prezime" + i,
                    "tel" + i, "skype"+i));
        }
    }

    private void fillKontaktView(Kontakt k, View v){
        TextView ime = v.findViewById(R.id.labelIme);
        TextView prezime = v.findViewById(R.id.labelPrezime);
        TextView tel = v.findViewById(R.id.labelTel);
        TextView skype = v.findViewById(R.id.labelSkype);
        ImageView img = v.findViewById(R.id.imageView);

        img.setImageResource(R.drawable.cat);

        ime.setText(k.getIme());
        prezime.setText(k.getPrezime());
        tel.setText(k.getBrojTelefona());
        skype.setText(k.getSkypeId());
        mainLayout.addView(v);
    }

    private void styleView(View v, int c){
        v.setBackgroundColor(c);
    }

    private void generateData(){
        LayoutInflater inflater = LayoutInflater.from(this);
        //LayoutInflater inflater = getLayoutInflater();
        boolean color = false;
        int colorValue = 0;

        for(Kontakt k : kontakti){
            View v = inflater.inflate(R.layout.kontakt_layout, mainLayout, false);
            fillKontaktView(k,v);

            if(color){
                colorValue = Color.CYAN;
                color = false;
            }else{
                colorValue = Color.LTGRAY;
                color = true;
            }
            styleView(v, colorValue);

        }
    }
}