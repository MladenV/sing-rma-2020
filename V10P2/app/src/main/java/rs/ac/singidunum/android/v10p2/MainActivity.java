package rs.ac.singidunum.android.v10p2;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity {

    private final static String APP_DATA_PREFIX = "MainActivityAppDataPrefix";
    private final static String APP_DATA_IME = "ime";
    private final static String APP_DATA_EMAIL = "email";

    private EditText inputIme;
    private EditText inputEmail;
    private Button buttonSave;
    private Button buttonLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init(){
        inputIme = (EditText) findViewById(R.id.inputIme);
        inputEmail = (EditText) findViewById(R.id.inputEmail);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

        buttonLoad = (Button) findViewById(R.id.buttonLoad);
        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void saveData(){
        String ime = inputIme.getText().toString();
        String email = inputEmail.getText().toString();
        try{
            FileOutputStream f = openFileOutput(APP_DATA_PREFIX, Context.MODE_PRIVATE);
            PrintWriter pw = new PrintWriter(f);
            pw.println(ime);
            pw.println(email);
            pw.flush();
            pw.close();
            f.close();
        }
        catch(Exception e)
        {

        }


    }

    private void loadData(){
        try {
            FileInputStream f = openFileInput(APP_DATA_PREFIX);
            BufferedReader bf = new BufferedReader(new InputStreamReader(f));

            String ime = bf.readLine();
            String email = bf.readLine();

            bf.close();
            f.close();

            inputIme.setText(ime);
            inputEmail.setText(email);
        }
        catch(Exception e){

        }


    }

}