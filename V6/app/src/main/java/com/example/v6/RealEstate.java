package com.example.v6;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class RealEstate {

    private int id;
    private String title;

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RealEstate(){

    }

    public RealEstate(int id, String title){
        this.id = id;
        this.title = title;
    }

    public static RealEstate fromJson(JSONObject json){
        RealEstate element = new RealEstate();

        try{
            if(json.has("id")){
                element.id = json.getInt("id");
            }
            if(json.has("title")){
                element.title = json.getString("title");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return element;
    }

    public static JSONObject toJson(RealEstate r){
        JSONObject o = new JSONObject();
        try {
            o.put("id", r.getId());
            o.put("title", r.getTitle());
        }catch(Exception e){
            e.printStackTrace();
        }
        return o;
    }

    public static ArrayList<RealEstate> fromJsonArray(JSONArray array){
        ArrayList<RealEstate> realestates = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                realestates.add(fromJson(array.getJSONObject(i)));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return realestates;
    }
}
