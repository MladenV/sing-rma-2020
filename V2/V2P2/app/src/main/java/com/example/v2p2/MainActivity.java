package com.example.v2p2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupListeners();
    }

    //Odvajamo podešavanje listenera u metodu, da rasteretimo onCreate i da rukujemo različitim orijentacijama
    private void setupListeners(){
        Button end = null;
        Button register = null;
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            end = findViewById(R.id.endButton);
            register = findViewById(R.id.regButton);
        }
        else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            end = findViewById(R.id.endButtonLandscape);
            register = findViewById(R.id.regButtonLandscape);
        }

        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivity();
            }
        });
    }

    private void changeActivity(){
        Intent i = new Intent(this, Prikaz.class);
        EditText e = findViewById(R.id.inputUname);
        String msg = e.getText().toString();

        //i.putExtra("message", msg);
        Bundle extras = new Bundle();
        extras.putString("message", msg);

        i.putExtras(extras);
        startActivity(i);
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_main_landscape);
        }
        else if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_main);
        }
        setupListeners();

    }


}