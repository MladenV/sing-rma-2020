package com.example.v2p2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class Prikaz extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prikaz);

        String msg = getIntent().getStringExtra("message");
        System.out.println(msg);
    }
}