package rs.ac.singidunum.android.v11;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView labelData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        initComponents();
    }

    private void initComponents(){
        labelData = (TextView) findViewById(R.id.labelData);

        Database db = new Database(this);
        OglasRepository repo = new OglasRepository(db);
        repo.addOglas("Prvi Novi Oglas", 250.2);

        labelData.setText(repo.getAllOglasi().toString());
    }
}
