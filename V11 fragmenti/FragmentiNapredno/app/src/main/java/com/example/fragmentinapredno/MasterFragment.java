package com.example.fragmentinapredno;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MasterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
//fragment za fakultete ne zna za druge fragmente iako se vrši komunikacija
//takodje ne koristi main activity kao cvoriste za komunikaciju
public class MasterFragment extends Fragment {
    ViewModelExample viewModel;

    public MasterFragment() {
        // Required empty public constructor
    }

    public static MasterFragment newInstance() {
        MasterFragment fragment = new MasterFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_master, container, false);
        //pri povezivanju na viewModel kao lifecycleOwner prosledjujemo activity na koji je vezan
        //tako se obezbedjuje da dobija isti viewModel kao i ostali fragmenti (ako svi na isti nacin koriste)
        viewModel = new ViewModelProvider(requireActivity()).get(ViewModelExample.class);
        LinearLayout ll = v.findViewById(R.id.fakultetiContainer);
        //kad god se azurira lista fakulteta, odnosno data svojstvo viewModela, poziva se metoda za iscrtavanje
        //odnosno azurira se UI
        viewModel.getData().observe(getViewLifecycleOwner(), data -> {
           drawData(data, ll);
        });

        return v;
    }

    private void drawData(List<String> data, ViewGroup container){
        container.removeAllViews();
        for(String s : data){
            TextView t = new TextView(requireActivity());
            t.setText(s);
            t.setTag(s); //nacin kako mozemo da pridruzimo podatke uz view, tag je objekat
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewModel.selectData((String)v.getTag());
                }
            });
            container.addView(t);
        }
    }
}