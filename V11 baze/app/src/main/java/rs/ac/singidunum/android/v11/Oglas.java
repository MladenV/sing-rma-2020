package rs.ac.singidunum.android.v11;

public class Oglas {

    public static final String TABLE_NAME="oglas";
    public static final String FIELD_OGLAS_ID="oglas_id";
    public static final String FIELD_NASLOV="naslov";
    public static final String FIELD_CENA="cena";

    private int oglasId;
    private String naslov;
    private double cena;

    public Oglas(int oglasId, String naslov, double cena) {
        this.oglasId = oglasId;
        this.naslov = naslov;
        this.cena = cena;
    }

    public int getOglasId() {
        return oglasId;
    }

    public void setOglasId(int contactId) {
        this.oglasId = contactId;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "Oglas{" +
                "oglasId=" + oglasId +
                ", naslov='" + naslov + '\'' +
                ", cena=" + cena +
                '}';
    }
}
