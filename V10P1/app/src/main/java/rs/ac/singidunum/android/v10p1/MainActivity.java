package rs.ac.singidunum.android.v10p1;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private final static String SHARED_PREFERENCES_PREFIX = "MainActivitySharedPreferencesPrefix";
    private final static String SHARED_PREFERENCES_IME = "ime";
    private final static String SHARED_PREFERENCES_EMAIL = "email";

    private EditText inputIme;
    private EditText inputEmail;
    private Button buttonSave;
    private Button buttonLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init(){
        inputIme = (EditText) findViewById(R.id.inputIme);
        inputEmail = (EditText) findViewById(R.id.inputEmail);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

        buttonLoad = (Button) findViewById(R.id.buttonLoad);
        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void saveData(){
        String ime = inputIme.getText().toString();
        String email = inputEmail.getText().toString();

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SHARED_PREFERENCES_IME, ime);
        editor.putString(SHARED_PREFERENCES_EMAIL, email);
        editor.commit(); //možemo da koristimo i apply, ako želimo u pozadini da se izvrši
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX, 0);
        String ime = sharedPreferences.getString(SHARED_PREFERENCES_IME, "Nije uneseno");
        String email = sharedPreferences.getString(SHARED_PREFERENCES_EMAIL, "Nije unesen");

        inputIme.setText(ime);
        inputEmail.setText(email);

        //Kod za brisanje sharedPreferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

}
